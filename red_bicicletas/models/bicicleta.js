// Definmos objeto
var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion
}

Bicicleta.prototype.toString = function (){
    return 'id: ' + this.id + " | color: " + this.color;
}

// Definmos método: Mostrar todas los items
Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici)

}

// Definmos método: Buscar bicis por "Id"
Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el Id $({aBiciId})`);
}

// Definmos método: Remover bicis por "Id"
Bicicleta.removeById = function(aBiciId){
    for(var i = 0; i < Bicicleta.allBicis.lenght; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}




var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.6030,-58.381]); 

// Nuestro modelo
Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;